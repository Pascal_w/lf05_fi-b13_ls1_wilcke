import java.util.Scanner;

class FahrkartenautomatEndlos {

    public static void main(String[] args) {

        char bestätigung;
        Scanner tastatur=new Scanner(System.in);
        do{
            double zuZahlenderBetrag = fahrkartenbestellungErfassen();

            double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            rueckgeldAusgeben(rückgabebetrag);

            fahrkartenAusgeben();
            fahrkartenNichtVergessen();

            System.out.println ("möchten sie weitere Tickets kaufen? ");
            bestätigung = tastatur.next().charAt(0);

        } while (bestätigung=='j'||bestätigung=='J');
        
        if(bestätigung=='n'||bestätigung=='N');
        System.out.println("bis zum nächsten mal ");
        
    }

    public static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        double[] ticketPreise = {0, 3.00, 2.70, 5.70};
        double gesamtpreis = 0.0;
        byte ticketauswahl;
        byte ticketanzahl = 0;
      do {  
        System.out.println ("bitte wählen sie Karte aus ");
        System.out.println ("Fahrkarte AB 3,00 EUR (1) ");
        System.out.println ("Fahrkarte BC 2,70 EUR (2) ");
        System.out.println ("Fahrkarte ABC 5,70 EUR (3) ");
        System.out.println ("wenn sie bezahlen wollen wählen Sie bitte die (6) ");
        System.out.println ("Ihre Wahl: ");
        ticketauswahl=tastatur.nextByte();

        //Falsche eingabe
        while (ticketauswahl != 6 && (ticketauswahl >= 4 || ticketauswahl <= 0)){
            System.out.println (" >>falsche Eingabe<< ");
            System.out.println ("Ihre Wahl: ");
            ticketauswahl = tastatur.nextByte();
        }
        
        //Abfrage Tickets wenn Auswahl nicht 6
        if (ticketauswahl != 6) {
            System.out.println ("anzahl der tickets ");
            ticketanzahl = tastatur.nextByte();
        }

        //Ticketanzahl
        while (ticketanzahl > 10  || ticketanzahl <= 0){
            System.out.println ("bitte wählen sie die Anzahl der Tickets zwichen 1 bis 10 ");
            System.out.println ("Anzahl der Tickets ");
            ticketanzahl = tastatur.nextByte();

        }
        //Berechnung der Ticketpreise
        if (ticketauswahl !=6){
            gesamtpreis += ticketPreise [ticketauswahl] * ticketanzahl;
            System.out.printf ("zwichensumme: %.2f €\n", gesamtpreis);

        }

        System.out.println("");
    } while (ticketauswahl != 6);



        
        return gesamtpreis;
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag = 0.0;

        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));

            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;

            rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        }
        return rückgabebetrag;
        
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {
        if (rückgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n", rückgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
        }
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void fahrkartenNichtVergessen() {
        System.out.println("Vergessen Sie Ihre Fahrkarten nicht" + "\n" + "Einen schönen Tag noch");
    }

}
