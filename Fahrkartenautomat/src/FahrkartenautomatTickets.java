import java.util.Scanner;

class FahrkartenautomatTickets {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		String option;
		Scanner op = new Scanner(System.in);

		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n");

			// Soll ein weiterer durchlauf durchgeführt werden?
			System.out.println("Wollen Sie einen erneuten Durchlauf starten? (J/N)");
			option = op.next();

		} while (option.contains("J") || option.contains("j"));

		System.out.println(" >>> Programm beendet <<< ");
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		boolean bezahlen = false;
        boolean isValid = true;
		byte ticketAuswahl;
		byte ticketAnzahl = 0;
		double gesamtpreis = 0.0;

		String[] ticketBeschreibung = {
				  "Einzelfahrschein Berlin AB"
				, "Einzelfahrschein Berlin BC" 
				, "Einzelfahrschein Berlin ABC"
				, "Kurzstrecke"
				, "Tageskarte Berlin AB"
				, "Tageskarte Berlin BC"
				, "Tageskarte Berlin ABC"
				, "Kleingruppen-Tageskarte Berlin AB"
				, "Kleingruppen-Tageskarte Berlin BC"
				, "Kleingruppen-Tageskarte Berlin ABC"};
		double[] ticketPreise = { 
				  2.90
				, 3.30
				, 3.60
				, 1.90
				, 8.60
				, 9.00
				, 9.60
				, 23.50
				, 24.30
				, 24.90};

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================");
		System.out.println("");

		
		do {
			System.out.println("Wählen Sie ihre Wunschfahrkarte aus: ");

			// Ausgabe der Array Elemente und Option zum bezahlen
			for(int i = 0; i < ticketPreise.length; i++) {
				System.out.printf("  %s [%.2f EUR] (%d)\n", ticketBeschreibung[i], ticketPreise[i], i+1);
			}
			System.out.printf("Bezahlen (%d)\n", ticketPreise.length+1);

			
			// Ticket auswählen
			System.out.print("Ihre Wahl: ");
			ticketAuswahl = tastatur.nextByte();

			// Wenn man bezahlt, wird bezahlen auf true gesetzen
			if (ticketAuswahl == ticketPreise.length+1) {
				bezahlen = true;
			}

            do {
                while (ticketAuswahl <= 0 || ticketAuswahl > ticketPreise.length+1) {
                    System.out.println(" >>falsche Eingabe<< ");
                    System.out.print("Ihre Wahl: ");
                    ticketAuswahl = tastatur.nextByte();
                }
                
                // Nur Anzahl der Tickets auswählen wenn wir nicht bezahlen wollen
                if (bezahlen != true) {
                    System.out.print("Anzahl der Tickets: ");
                    ticketAnzahl = tastatur.nextByte();
                }
    
                // Ticketanzahl darf nur zwischen 0 und 10 sein
                if (ticketAnzahl > 10 || ticketAnzahl < 0) {
                    System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
                    System.out.print("Anzahl der Tickets: ");
                    ticketAnzahl = tastatur.nextByte();
                }
            } while (!isValid);
			
			// Nur in range vom Array Auswahlmöglichkeiten anbieten 
			

			// Nur Zwischensumme und Preis bestimmen, wenn wir ein Ticket ausgewählt haben
			if (bezahlen != true) {
				gesamtpreis += ticketPreise[ticketAuswahl-1] * ticketAnzahl;
				System.out.printf("Zwischensumme: %.2f €\n", gesamtpreis);
			}
			
			// Zeilenumbruch
			System.out.println("");
		} while (bezahlen != true);

		return gesamtpreis;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2.00 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
        tastatur.close();

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		String einheit = "Euro";
		muenzeAusgeben(rückgabebetrag, einheit);
	}

	// Methode zum warten einer gewissen Zeit in Millisekunden
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Ausgabe der Muenzen in eigener Methode
	public static void muenzeAusgeben(double betrag, String einheit) {
		if (betrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f %s \n", betrag, einheit);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (betrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.printf("2 %s \n", einheit);
				betrag -= 2.0;
			}
			while (betrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.printf("1 %s \n", einheit);
				betrag -= 1.0;
			}
			while (betrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.printf("0.50 %s \n", einheit);
				betrag -= 0.5;
			}
			while (betrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.printf("0.20 %s \n", einheit);
				betrag -= 0.2;
			}
			while (betrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.printf("0.10 %s \n", einheit);
				betrag -= 0.1;
			}
			while (betrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.printf("0.05 %s \n", einheit);
				betrag -= 0.05;
			}
		}
	}
}