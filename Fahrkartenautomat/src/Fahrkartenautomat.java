﻿import java.util.Scanner;

class Fahrkartenautomat {

  public static void main(String[] args) {
    while (true) {
      double zuZahlenderBetrag = fahrkartenbestellungErfassen();

      double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

      rueckgeldAusgeben(rückgabebetrag);

      fahrkartenAusgeben();
      fahrkartenNichtVergessen();
    }
  }

  public static double fahrkartenbestellungErfassen() {
    Scanner tastatur = new Scanner(System.in);
    double zuZahlenderBetrag;
    double ticketPreis;

    System.out.print("Ticketpreis (EURO): ");
    ticketPreis = tastatur.nextDouble();

    System.out.print("Anzahl der Tickets: ");
    short tickets = tastatur.nextShort();

    zuZahlenderBetrag = ticketPreis * tickets;
    return zuZahlenderBetrag;
  }

  public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    Scanner tastatur = new Scanner(System.in);
    double eingezahlterGesamtbetrag;
    double eingeworfeneMünze;
    double rückgabebetrag = 0.0;

    eingezahlterGesamtbetrag = 0.0;
    while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
      System.out.printf(
        "Noch zu zahlen: %.2f Euro \n",
        (zuZahlenderBetrag - eingezahlterGesamtbetrag)
      );

      System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      eingeworfeneMünze = tastatur.nextDouble();
      eingezahlterGesamtbetrag += eingeworfeneMünze;

      rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    return rückgabebetrag;
  }

  public static void rueckgeldAusgeben(double rückgabebetrag) {
      if (rückgabebetrag > 0.0) {
      System.out.printf(
        "Der Rückgabebetrag in Höhe von %.2f Euro \n",
        rückgabebetrag
      );
      }
      
      System.out.println("wird in folgenden Münzen ausgezahlt:");

      while (
        rückgabebetrag >= 2.0
      ) { // 2 EURO-Münzen
        System.out.println("2 EURO");
        rückgabebetrag -= 2.0;
      }
      while (
        rückgabebetrag >= 1.0
      ) { // 1 EURO-Münzen
        System.out.println("1 EURO");
        rückgabebetrag -= 1.0;
      }
      while (
        rückgabebetrag >= 0.5
      ) { // 50 CENT-Münzen
        System.out.println("50 CENT");
        rückgabebetrag -= 0.5;
      }
      while (
        rückgabebetrag >= 0.2
      ) { // 20 CENT-Münzen
        System.out.println("20 CENT");
        rückgabebetrag -= 0.2;
      }
      while (
        rückgabebetrag >= 0.1
      ) { // 10 CENT-Münzen
        System.out.println("10 CENT");
        rückgabebetrag -= 0.1;
      }
      while (
        rückgabebetrag >= 0.05
      ) { // 5 CENT-Münzen
      while (rückgabebetrag >= 2.0) { // 2 EURO-Münzen
        System.out.println("2 EURO");
        rückgabebetrag -= 2.0;
      }
      while (rückgabebetrag >= 1.0) { // 1 EURO-Münzen
        System.out.println("1 EURO");
        rückgabebetrag -= 1.0;
      }
      while (rückgabebetrag >= 0.5) { // 50 CENT-Münzen
        System.out.println("50 CENT");
        rückgabebetrag -= 0.5;
      }
      while (rückgabebetrag >= 0.2) { // 20 CENT-Münzen
        System.out.println("20 CENT");
        rückgabebetrag -= 0.2;
      }
      while (rückgabebetrag >= 0.1) { // 10 CENT-Münzen
        System.out.println("10 CENT");
        rückgabebetrag -= 0.1;
      }
      while (rückgabebetrag >= 0.05) { // 5 CENT-Münzen
        System.out.println("5 CENT");
        rückgabebetrag -= 0.05;
      }
    }
      
  }

  public static void fahrkartenAusgeben() {
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++) {
      System.out.print("=");
      try {
        Thread.sleep(250);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    System.out.println("\n\n");
  }


  public static void fahrkartenNichtVergessen() {
    System.out.println(
      "Vergessen Sie Ihre Fahrkarten nicht" + "\n" + "Einen schönen Tag noch"
    );
  }

}
