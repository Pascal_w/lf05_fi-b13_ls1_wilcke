import java.util.*;
 
public class AB_Auswahlstrukturen_BMI {
  
  public static void main(String[] args) {
    
    Scanner einlesen = new Scanner(System.in);
    int wert, wert2; 
    String ausgabe = "";
    String eingabe;
    
    double kilogramm;
    double kilogramm_bmi;
    double gewicht; 
    String geschlecht;
    double bmi;
    boolean maennlich;
    
    eingabe="Bitte geben Sie Ihre Koerpergroesse an in cm: ";
    System.out.println("");
    System.out.print(eingabe);
    kilogramm = einlesen.nextDouble();
    eingabe="Bitte geben Sie jetzt Ihr Gewicht an (in kg):";
    System.out.print(eingabe);
    gewicht = einlesen.nextDouble();
    eingabe="Geben Sie bitte Ihr Geschlecht an (M/W):";
    System.out.print(eingabe);
    geschlecht = einlesen.next();
    
    
    kilogramm_bmi = kilogramm/100;
    bmi = gewicht/(kilogramm_bmi*kilogramm_bmi);
    
    if (geschlecht.contains("m")){
      if (bmi<20){
        ausgabe="Sie haben Untergewicht " + bmi;
      }
      else if(bmi>=20 && bmi<=25){
        ausgabe="Sie haben Normalgewicht " + bmi;
      }
      else{
        ausgabe="Sie haben Uebergewicht " + bmi;
      }
    }
    else if (geschlecht.contains("w")){
      if (bmi<19){
        ausgabe="Sie haben Untergewicht " + bmi;
      }
      else if(bmi>=19 && bmi<=24){
        ausgabe="Sie haben Normalgewicht " + bmi;
      }
      else{
        ausgabe="Sie haben Uebergewicht " + bmi;
      }
    }
    else{
      ausgabe="Das ist kein Geschlecht";
    }
    
    System.out.println("");
    System.out.println(ausgabe);    
    
    
  }
}
  
