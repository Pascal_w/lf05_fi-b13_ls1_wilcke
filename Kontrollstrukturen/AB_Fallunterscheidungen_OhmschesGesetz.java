import java.util.Scanner;

public class AB_Fallunterscheidungen_OhmschesGesetz {

	public static void main(String[] args) {
		char auswahl;
		double u, i, r;
		Scanner sc = new Scanner(System.in);

		System.out.println("Was moechten Sie berechnen (U, I, R): ");
		auswahl = sc.next().charAt(0);
		switch (auswahl)
		{
			case 'R':
			case 'r':
				System.out.println("Spannung in Volt eingeben: ");
				u = sc.nextDouble();
				System.out.println("Strom in Ampere eingeben: ");
				i = sc.nextDouble();
				r = u / i;
				System.out.printf("R = %.2f Ohm", r);
				break;
			case 'U':
			case 'u':
				System.out.println("Strom in Ampere eingeben: ");
				i = sc.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: ");
				r = sc.nextDouble();
				u = i * r;
				System.out.printf("U = %.2f Volt", u);
				break;
			case 'I':
			case 'i':
				System.out.println("Spannung in Volt eingeben: ");
				u = sc.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: ");
				r = sc.nextDouble();
				i = u / r;
				System.out.printf("I = %.2f Ampere", i);
				break;
			default:
				System.out.println("Falsche Auswahl!");
				break;
		}

	}

}
