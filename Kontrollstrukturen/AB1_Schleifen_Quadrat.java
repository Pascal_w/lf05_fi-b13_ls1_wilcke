import java.util.Scanner;

public class AB1_Schleifen_Quadrat {

	public static void main(String[] args) {

		
		Scanner eingabe = new Scanner(System.in);
		
		System.out.print("Geben Sie die Laenge des Quadrates an: ");
		int size = eingabe.nextInt() - 1;

		System.out.println("");
		
		for (int x = 0; x <= size; x++) {
			
			for (int y = 0; y <= size; y++) {
				
				if (x == 0 || x == size || y == 0 || y == size) {
					System.out.printf("%2s", "*");
				} 
				else {
					System.out.print("  ");
				}
			}
			System.out.print("\n");
		}

		
		eingabe.close();
		
	}

}
