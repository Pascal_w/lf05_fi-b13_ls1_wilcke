import java.util.Scanner;

public class AB2_Schleifen_Million {

	public static void main(String[] Args) {

		Scanner sc = new Scanner(System.in);
		char eingabe = 'j';
		double anlageKapital, zinssatz;
		int i = 1;

		while (eingabe == 'j') {

			System.out.print("Bitte geben Sie an, wie viel Kapital Sie anlegen moechten: ");
			anlageKapital = sc.nextDouble();

			System.out.print("Bitte geben Sie den Zinssatz an: ");
			zinssatz = sc.nextDouble();
			zinssatz = zinssatz / 100 + 1;

		
			while (anlageKapital < 1000000) {
				anlageKapital = anlageKapital * zinssatz;
				i++;
			}
			
            System.out.println("Nach " + i + " Jahren ueberschreitet das Anlagekapitel die Schwelle von 1.000.000 Euro");
			
			System.out.println("Moechten Sie einen weiteren Durchlauf starten? (j/n)");
			eingabe = sc.next().charAt(0);
		}
		sc.close();
	}
}