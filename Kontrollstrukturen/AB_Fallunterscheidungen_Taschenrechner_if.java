import java.util.Scanner;


public class AB_Fallunterscheidungen_Taschenrechner_if {
  
  public static void main(String[] args) {
    
    char operator = ' ';
    double zahl1, zahl2, erg = 0.0;
    
    Scanner tastatur = new Scanner(System.in);
    
    System.out.println("Dieser Taschenrechner addiert, subtrahiert, multipliziert oder dividiert zwei eingegebenen Zahlen.");
    System.out.println("Bitte geben Sie die erste Zahl ein: ");
    zahl1 = tastatur.nextDouble();
    System.out.println("Bitte eben Sie die zweite Zahl ein: ");
    zahl2 = tastatur.nextDouble();
    
    System.out.print("Was soll berechnet werden. ");
    System.out.println("Bitte geben Sie einer der folgenden Zeichen ein: ");
    System.out.println("(+) Addition");
    System.out.println("(-) Subtraktion");
    System.out.println("(*) Multiplikation");
    System.out.println("(/) Division");
    operator = tastatur.next().charAt(0);
    
    if (operator == '+') {
      erg = zahl1 + zahl2;
      System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
    } 
    else if (operator == '-') {
        erg = zahl1 - zahl2;
        System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
    }
    else if (operator == '*') {
          erg = zahl1 * zahl2;
          System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
    }
    else if (operator == '/') {
             erg = zahl1 / zahl2;
            System.out.println(zahl1 + " " + operator + " " + zahl2 + " = " + Math.round(erg * 100)/100.0 );
    }
    else {
      System.out.println("Sie haben keinen gueltigen Operator eingegeben!!");
    }
    
    
    
    
    
  }

}

