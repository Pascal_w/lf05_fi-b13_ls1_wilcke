import java.util.Scanner; // Import der Klasse Scanner 
public class test  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);   
     
    System.out.print("Bitte geben Sie Ihren Namen ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    String name = myScanner.next();  
     
    // Alter des users erfragen
    System.out.print("Bitte geben Sie Ihr Alter an: ");
    int alter = myScanner.nextInt(); 
     
    System.out.println("Hallo " + name); 
    System.out.print("Dein Alter ist: " + alter);   
 
    myScanner.close(); 
     
  }
} 