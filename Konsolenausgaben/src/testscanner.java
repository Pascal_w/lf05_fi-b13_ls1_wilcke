import java.util.Scanner;

public class testscanner {

    public static void main(String[]args) {
        
        Scanner myScanner = new Scanner(System.in);

        System.out.print("11. ");

        int zahl1 = myScanner.nextInt();

        System.out.print("5: ");

        int zahl2 = myScanner.nextInt();

        int ergebnis = zahl1 + zahl2;

        System.out.print("\n\n\nErgebnis der Addition lautet: ");
        System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);

        myScanner.close();
    }
}
