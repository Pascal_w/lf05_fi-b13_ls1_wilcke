import java.util.Arrays;

public class array {

  public static void main(String args[]) {
    int[] array1 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    System.out.println("First array is:");
    for (int i = 0; i < array1.length; i++) {
      System.out.println(array1[i]);
    }

    int[] array2 = Arrays.copyOf(array1, 10);
    array2[5] = 6;
    array2[6] = 7;

    System.out.println("New array after copying elements is:");
    for (int i = 0; i < array2.length; i++) {
      System.out.println(array2[i]);
    }
  }
}
