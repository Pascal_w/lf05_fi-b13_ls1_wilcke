public class Aufgabe3Temperaturtabelle {

    public static void main(String[] args){
    
        String a = "Fahrenheit";
        String b = "-----------------------";
        int c = 20;
        int d = 10;
        int e = 0;
        int g = 30;
        String h = "Celsius";
        double i = 28.8889;
        double j = 23.3333;
        double k = 17.7778;
        double l = 6.6667;
        double m = 1.1111;
        


        System.out.printf("%-12s|%10s", a,h);
        System.out.printf("\n%s\n", b);
        System.out.printf("%-12s|%10.2f\n",c,-i);
        System.out.printf("%-12s|%10.2f\n",d,-j);
        System.out.printf("%-12s|%10.2f\n",e,-k);
        System.out.printf("%-12s|%10.2f\n","+"+c,-l);
        System.out.printf("%-12s|%10.2f\n","+"+g,-m);

    }
}