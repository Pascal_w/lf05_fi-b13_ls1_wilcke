package Kontoaufgabe;

public class Konto {

	//attributes
	private String iban;
	private int kontonummer;
	private int kontostand = 0;
	private Kontobesitzer besitzer;
	
	
	//konstruktor
	public Konto(String iban) {
		this.iban = iban;
	}
	
	public Konto(String iban, int kontonummer, int kontostand, Kontobesitzer besitzer) {
		super();
		this.iban = iban;
		this.kontonummer = kontonummer;
		this.kontostand = kontostand;
		this.besitzer = besitzer;
	}

	
	//getter und setter
	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public int getKontonummer() {
		return kontonummer;
	}

	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}

	public int getKontostand() {
		return kontostand;
	}

	public void setKontostand(int kontostand) {
		this.kontostand = kontostand;
	}

	public Kontobesitzer getBesitzer() {
		return besitzer;
	}

	public void setBesitzer(Kontobesitzer besitzer) {
		this.besitzer = besitzer;
	}
	
	//methoden

	public void einzahlen(int gezahlterBetrag){
		this.kontostand += gezahlterBetrag;
	}

	public void geldUeberweisen(int betrag, Konto anderesKonto) {
		if(anderesKonto != null) {
			if(this.kontostand >= betrag) {
				this.kontostand -= betrag;
				anderesKonto.kontostand += betrag;
			} else {
				throw new RuntimeException("Nicht genuegend Geld auf Ihrem Konto");
			}
			
		} else {
			throw new RuntimeException("Kein Konto mit dieser IBAN existiert");
		}
	}
	
}
