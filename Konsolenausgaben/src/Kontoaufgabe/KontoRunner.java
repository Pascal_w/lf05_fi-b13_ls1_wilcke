package Kontoaufgabe;

public class KontoRunner {

	public static void main(String[] args) {

		Konto konto1 = new Konto("DE510000000000");
		Konto konto2 = new Konto("DE510000000001");
		Kontobesitzer besitzer1 = new Kontobesitzer("Waskoenig", "Joerg", new Konto[]{konto1, konto2}, name);
		
		konto1.setKontostand(20000);
		konto1.geldUeberweisen(10000, konto2);

		konto1.einzahlen(66666);
		
		
	
		System.out.println(konto1.getKontostand());
		System.out.println(konto2.getKontostand());
		
	}

}
