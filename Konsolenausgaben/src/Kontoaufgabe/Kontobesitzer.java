package Kontoaufgabe;

public class Kontobesitzer {

	//attribute
	private String name;
	private String vorname;
	private Konto konten[] = new Konto[2];


	//konstruktor
	public Kontobesitzer() {

	};

	public Kontobesitzer(String name, String vorname, Konto[] konten) {
		super();
		this.name = name;
		this.vorname = vorname;
		this.konten = konten;
	}


	//getter und setter

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public Konto[] getKonten() {
		return konten;
	}

	public void setKonten(Konto[] konten) {
		this.konten = konten;
	}

	//methoden
	public Konto[] gesamtUebersicht() {
		return this.getKonten();
	}

	public int gesamtGeld() {
		int gesamtGeld = 0;
		for(int i = 0; i < this.konten.length; i++) {
			gesamtGeld += this.konten[i].getKontostand();
		}
		return gesamtGeld;
	}

}
