package Raumschiff;

public class Ladung {

    // Attribute

    private String bezeichnung;
    private int menge;

    // Getter Setter

    public Ladung() {

    }

    public String getBezeichnung() {
        return this.bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getMenge() {
        return this.menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

    // Konstruktor

    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }

    @Override
    public String toString() {
        return "{" +
                " bezeichnung='" + getBezeichnung() + "'" +
                ", menge='" + getMenge() + "'" +
                "}";
    }

}
